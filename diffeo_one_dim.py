import os
import numpy as np
import torch
import matplotlib.pyplot as plt
from torch import nn
from abstract_diffeo import AbstractDiffeo
from utils import uniform_density, normal_density, cauchy_density, uniform_density_cdf, \
    cauchy_density_cdf, normal_density_cdf, plot_probability_density, get_probability_density, \
    inverse_erf_derivative, plot_diffeo, bimodal_density, bimodal_density_cdf, multimodal_density, multimodal_density_cdf


# Then, sort out the precise link between push-forward and normal distribution
# assuming diffeomorphic parametrization
# Is it useful to solve the problem (at least numerically) in high dim ?
# Think about the implications for GANS: are there some distributions they cannot
# capture

class UniformDiffeo(AbstractDiffeo):

    def __init__(self, f, mu=0., cdf=None, x_min=-8., x_max=8.):
        super(UniformDiffeo, self).__init__(x_min, x_max)
        self.mu_float = mu
        self.mu_torch = torch.from_numpy(np.array([mu])).double()
        self.f = f
        self.f_mu = f(self.mu_torch)
        self.cdf = cdf

    def metric(self, x):
        return self.derivative(x) ** 2

    def _integrate(self, x):
        assert self.cdf is not None
        return self.cdf(x) - self.cdf(self.mu_torch)

    def __call__(self, x):
        return self._integrate(x) / self.f_mu

    def derivative(self, x):
        return self.f(x) / self.f_mu


class TheoreticalDiffeo(AbstractDiffeo):

    def __init__(self, f, mu=0., cdf=None, x_min=-8., x_max=8.):
        super(TheoreticalDiffeo, self).__init__(x_min, x_max)
        self.mu_float = mu
        self.mu_torch = torch.from_numpy(np.array([mu])).double()
        self.f = f
        self.f_mu = f(self.mu_torch)
        self.gamma_torch = 2 * np.pi * self.f_mu ** 2
        print('Gamma set to:', self.gamma_torch.detach().numpy())
        self.step = 1e-5
        self.cdf = cdf
        self.pi = np.pi * torch.ones(1).double()
        self.values = None
        self.points = None
        self.update()

    def _integrate(self, x):
        """
        returns the numerical integral of f from mu to x, or the analytical one if provided.
        """
        if self.cdf is not None:
            out = self.cdf(x) - self.cdf(self.mu_torch)
            return out  # assuming symmetric distribution here.
        else:
            float_x = float(x.detach().numpy()[0])
            if torch.norm(x - self.mu_torch) < self.step:
                return torch.zeros(1).double()
            if float_x >= self.mu_float + self.step:
                points = torch.arange(self.mu_float, float_x, self.step).double()
                return self.step * torch.sum(self.f(points))
            else:
                points = torch.arange(float_x + self.step, self.mu_float, self.step).double()
                return -1. * self.step * torch.sum(self.f(points))

    def metric(self, x):
        if torch.norm(x - self.mu_torch) < self.step:
            return torch.ones(1).double()
        else:
            return self.derivative(x)**2

    def inverse_metric(self, x):
        return 1./ self.metric(x)

    def hamiltonian(self, x, p):
        return 0.5 * self.inverse_metric(x) * p ** 2

    def __call__(self, x):
        return torch.sqrt(2/self.gamma_torch) \
               * torch.erfinv(torch.sqrt(2*self.gamma_torch/self.pi) * self._integrate(x) / self.f_mu)

    def derivative(self, x):
        return 2/torch.sqrt(self.pi) * self.f(x)/self.f_mu \
               * inverse_erf_derivative(torch.sqrt(2*self.gamma_torch/self.pi) * self._integrate(x) / self.f_mu)


legend_size=15
title_size=20

if __name__ == '__main__':
    # theoretical_diffeo = TheoreticalDiffeo(uniform_density, mu=0.5, cdf=uniform_density_cdf)
    # theoretical_diffeo.plot_metric()
    # plt.savefig('uniform_distribution_metric.pdf')
    # plt.show()
    # integral = plot_probability_density(theoretical_diffeo, pdf=uniform_density, mu=0.5)
    # plt.xlim(-1, 2)
    # print('Integral:', integral)
    # plt.savefig('uniform_distribution_density.pdf')
    # plt.show()
    # plot_diffeo(theoretical_diffeo, label='Diffeomorphism for uniform distribution')
    # plt.xlim(-1, 2)
    # plt.ylim(-4, 4)
    # plt.savefig('uniform_distribution_diffeo.pdf', bbox_inches='tight', pad_inches=0)
    # plt.show()
    #
    theoretical_diffeo = TheoreticalDiffeo(normal_density, mu=0., cdf=normal_density_cdf)
    theoretical_diffeo.plot_metric(title_size=title_size)
    plt.xlim(-3, 3)
    plt.legend(prop={'size':legend_size})
    plt.savefig('normal_distribution_metric.pdf', bbox_inches='tight', pad_inches=0)
    plt.show()
    integral = plot_probability_density(theoretical_diffeo, pdf=normal_density, rescale=False, title_size=title_size)
    print('Integral:', integral)
    plt.xlim(-4, 4)
    plt.legend(prop={'size': legend_size})
    plt.savefig('normal_distribution_density.pdf', bbox_inches='tight', pad_inches=0)
    plt.show()
    plot_diffeo(theoretical_diffeo, label='normal', title_size=title_size)
    plt.xlim(-4, 4)
    plt.ylim(-4, 4)
    plt.legend(prop={'size': legend_size})
    plt.savefig('normal_distribution_diffeo.pdf', bbox_inches='tight', pad_inches=0)
    plt.show()


    theoretical_diffeo = TheoreticalDiffeo(cauchy_density, cdf=cauchy_density_cdf)
    theoretical_diffeo.plot_metric(title_size=title_size)
    plt.xlim(-4, 4)
    plt.legend(prop={'size': legend_size})
    plt.savefig('cauchy_distribution_metric.pdf', bbox_inches='tight', pad_inches=0)
    plt.show()
    integral = plot_probability_density(theoretical_diffeo, pdf=cauchy_density, title_size=title_size)
    print('Integral:', integral)
    plt.xlim(-4, 4)
    plt.legend(prop={'size': legend_size})
    plt.savefig('cauchy_distribution_density.pdf', bbox_inches='tight', pad_inches=0)
    plt.show()
    plot_diffeo(theoretical_diffeo, label='cauchy', title_size=title_size)
    plt.xlim(-4, 4)
    plt.ylim(-4, 4)
    plt.legend(prop={'size': legend_size})
    plt.savefig('cauchy_distribution_diffeo.pdf', bbox_inches='tight', pad_inches=0)
    plt.show()

    theoretical_diffeo = TheoreticalDiffeo(bimodal_density, cdf=bimodal_density_cdf)
    theoretical_diffeo.plot_metric(title_size=title_size)
    plt.xlim(-4, 4)
    plt.legend(prop={'size': legend_size})
    plt.savefig('bimodal_distribution_metric.pdf', bbox_inches='tight', pad_inches=0)
    plt.show()
    integral = plot_probability_density(theoretical_diffeo, pdf=bimodal_density, title_size=title_size)
    print('Integral:', integral)
    plt.xlim(-4, 4)
    plt.legend(prop={'size': legend_size})
    plt.savefig('bimodal_distribution_density.pdf', bbox_inches='tight', pad_inches=0)
    plt.show()
    plot_diffeo(theoretical_diffeo, label='bimodal', title_size=title_size)
    plt.xlim(-4, 4)
    plt.ylim(-4, 4)
    plt.legend(prop={'size': legend_size})
    plt.savefig('bimodal_distribution_diffeo.pdf', bbox_inches='tight', pad_inches=0)
    plt.show()


    theoretical_diffeo = TheoreticalDiffeo(multimodal_density, cdf=multimodal_density_cdf)
    theoretical_diffeo.plot_metric(title_size=title_size)
    plt.xlim(-4, 4)
    plt.legend(prop={'size': legend_size})
    plt.savefig('multimodal_distribution_metric.pdf', bbox_inches='tight', pad_inches=0)
    plt.show()
    integral = plot_probability_density(theoretical_diffeo, pdf=multimodal_density, title_size=title_size)
    print('Integral:', integral)
    plt.xlim(-4, 4)
    plt.legend(prop={'size': legend_size})
    plt.savefig('multimodal_distribution_density.pdf', bbox_inches='tight', pad_inches=0)
    plt.show()
    plot_diffeo(theoretical_diffeo, label='multimodal', title_size=title_size)
    plt.xlim(-4, 4)
    plt.ylim(-4, 4)
    plt.legend(prop={'size': legend_size})
    plt.savefig('mltimodal_distribution_diffeo.pdf', bbox_inches='tight', pad_inches=0)
    plt.show()



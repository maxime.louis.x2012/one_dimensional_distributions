import os
import numpy as np
import torch
import matplotlib.pyplot as plt
from kernel_density_estimation import DensityEstimator
from diffeo_one_dim import TheoreticalDiffeo
from utils import plot_probability_density, get_probability_density, plot_diffeo
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error


# implement the geodesic regression with L2 residuals !!!! Otherwise there is not way it beats the
# linear regression... Do it smartly.
# open question: is geodesic regression equivalent to a gaussian generative model on the manifold ? Interesting one !


def get_antecedants(diffeo, observations):
    antecedants = torch.zeros_like(observations)
    for i in range(len(observations)):
        antecedants[i] = diffeo(observations[i])
    return antecedants.detach().numpy()


def geodesic_regression_l2_residuals(phi, times, observations, plot=False):
    # WORK IN PROGRESS, NOT SATISFYING FOR NOW
    times_torch = torch.from_numpy(times)
    observations_torch = torch.from_numpy(observations)

    a = torch.zeros(1).double()
    b = torch.mean(observations_torch)
    a.requires_grad_(True)
    b.requires_grad_(True)
    nb_epochs = 100
    optimizer = torch.optim.Adam([a, b], lr=1e-2)
    for epoch in range(nb_epochs):

        optimizer.zero_grad()

        loss = torch.zeros(1).double()
        for k, (t, obs) in enumerate(zip(times_torch, observations_torch)):
            loss += (phi.inverse(a * t + b) - obs)**2
        loss /= len(observations_torch)

        print('Loss:', loss.detach().numpy(), a.detach().numpy(), b.detach().numpy())

        loss.backward()
        optimizer.step()

    if plot:
        step = (np.max(times) - np.min(times)) / 100.
        times_for_plot = torch.arange(np.min(times), np.max(times), step).double()
        values_for_plot = torch.zeros_like(times_for_plot)
        for k, t in enumerate(times_for_plot):
            values_for_plot[k] = phi.inverse(a * t + b)
        plt.plot(times_for_plot.detach().numpy(), values_for_plot.detach().numpy())
        plt.scatter(times, observations)
        plt.title('Geodesic regression with l2 residuals')
        plt.show()

    return loss.detach().numpy()


# This is the intrinsic geodesic regression i.e. with geodesic distance residuals.
def geodesic_regression(phi, times, observations, plot=False):
    step = (np.max(times) - np.min(times))/100.
    times_for_plot = np.arange(np.min(times), np.max(times), step)

    # first: usual linear regression
    linear_regression = LinearRegression()
    linear_regression.fit(times.reshape(-1, 1), observations.reshape(-1, 1))
    linear_predictions = linear_regression.predict(times.reshape(-1, 1)).flatten()
    mse_linear = mean_squared_error(linear_predictions, observations)
    linear_predictions_for_plot = linear_regression.predict(times_for_plot.reshape(-1, 1))

    # Then, linear regression in latent space:
    antecedants = get_antecedants(phi, torch.from_numpy(observations))
    linear_regression = LinearRegression()
    linear_regression.fit(times.reshape(-1, 1), antecedants.reshape(-1, 1))
    latent_predictions = linear_regression.predict(times.reshape(-1, 1)).flatten()
    image_predictions = np.array([phi.inverse(elt, mode='approx') for elt in latent_predictions])
    mse_geodesic = mean_squared_error(observations, image_predictions)

    if plot:
        predictions_for_plot = [phi.inverse(elt) for elt in linear_regression.predict(times_for_plot.reshape(-1, 1))]
        plt.scatter(times, observations, label='Observations', alpha=0.5)
        plt.plot(times_for_plot, predictions_for_plot, label='Geodesic regression', color='red')
        plt.plot(times_for_plot, linear_predictions_for_plot, label='Linear regression', color='blue')
        plt.legend()
        plt.show()

    return mse_linear, mse_geodesic


# Now on real data:
volumes = np.loadtxt('volumes.txt')
ages = np.loadtxt('ages.txt')
rids = np.loadtxt('rids.txt')

volumes = volumes-np.mean(volumes)
volumes /= np.std(volumes)

# We gather some series for subjects...
ids = []
volumes_series = []
ages_series = []

for rid in set(rids):
    ids.append(rid)
    aux_age = []
    aux_volume = []
    for (i, vol, age) in zip(rids, volumes, ages):
        if i == rid:
            aux_age.append(age)
            aux_volume.append(vol)
    if len(aux_age) >= 2:
        volumes_series.append(np.array(aux_volume))
        ages_series.append(np.array(aux_age))


volumes_torch = torch.from_numpy(volumes)
ages_torch = torch.from_numpy(ages)


density_estimator = DensityEstimator(volumes_torch)
density_estimator.plot()

title_size=20
legend_size=15

mu = density_estimator.get_median()
print('Estimated median of the estimated distribution:', mu)
theoretical_diffeo = TheoreticalDiffeo(density_estimator.pdf, cdf=density_estimator.cdf, mu=mu, x_min=density_estimator.min, x_max=density_estimator.max)
_ = plot_probability_density(theoretical_diffeo, pdf=density_estimator.pdf, mu=mu, title_size=title_size)
plt.xlim(-4, 4)
plt.legend(prop={'size':legend_size})
plt.savefig('from_samples_density.pdf', bbox_inches='tight', pad_inches=0)
plt.show()

theoretical_diffeo.plot_metric(title_size=title_size)
plt.xlim(-4, 4)
plt.ylim(0, 10)
plt.legend(prop={'size':legend_size})
plt.savefig('from_samples_metric.pdf', bbox_inches='tight', pad_inches=0)
plt.show()


plot_diffeo(theoretical_diffeo, label='Hippocampus Volumes', title_size=title_size)
plt.xlim(-4, 4)
plt.legend(prop={'size':legend_size})
plt.savefig('from_samples_diffeo.pdf', bbox_inches='tight', pad_inches=0)
plt.show()


# print(geodesic_regression(theoretical_diffeo, ages, volumes))

# geodesic_regression_l2_residuals(theoretical_diffeo, ages, volumes, plot=True)
#
# mses_linear, mses_geodesic, mses_geodesic_l2 = [], [], []
#
# for ags, vols in zip(ages_series[:5], volumes_series[:5]):
#     if len(ags) >= 5:
#         mse_linear, mse_geodesic = geodesic_regression(theoretical_diffeo, ags, vols, plot=False)
#         mse_geodesic_l2 = geodesic_regression_l2_residuals(theoretical_diffeo, ags, vols, plot=True)
#         mses_linear.append(mse_linear)
#         mses_geodesic.append(mse_geodesic)
#         mses_geodesic_l2.append(mse_geodesic_l2)
#
# print(np.mean(mses_linear), '+/-', np.std(mses_linear))
# print(np.mean(mses_geodesic), '+/-', np.std(mses_geodesic))
# print(np.mean(mses_geodesic_l2), '+/-', np.std(mses_geodesic_l2))

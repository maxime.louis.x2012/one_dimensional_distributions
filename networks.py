from torch import nn


class Generator(nn.Module):

    def __init__(self, in_dim=1, out_dim=1, hidden_dim=32):
        super(Generator, self).__init__()
        self.layers = nn.ModuleList([nn.Linear(in_dim, hidden_dim),
                                     nn.ReLU(),
                                     nn.Linear(hidden_dim, hidden_dim),
                                     nn.ReLU(),
                                     # nn.Linear(hidden_dim, hidden_dim),
                                     # nn.ReLU(),
                                     nn.Linear(hidden_dim, hidden_dim),
                                     nn.ReLU(),
                                     nn.Linear(hidden_dim, out_dim)
                                     ])
        self.double()

    def forward(self, x):
        for layer in self.layers:
            x = layer(x)
        return x


class Critic(nn.Module):

    def __init__(self, in_dim=1, hidden_dim=32):
        super(Critic, self).__init__()
        self.layers = nn.ModuleList([nn.Linear(in_dim, hidden_dim),
                                     nn.ReLU(),
                                     nn.Linear(hidden_dim, hidden_dim),
                                     nn.ReLU(),
                                     nn.Linear(hidden_dim, hidden_dim),
                                     nn.ReLU(),
                                     nn.Linear(hidden_dim, 1)
                                     ])
        self.double()

    def forward(self, x):
        for layer in self.layers:
            x = layer(x)
        return x

class Encoder(nn.Module):

    def __init__(self, in_dim=10, hidden_dim=32, out_dim=2):
        super(Encoder, self).__init__()
        self.layers = nn.ModuleList([nn.Linear(in_dim, hidden_dim),
                                     nn.ReLU(),
                                     nn.Linear(hidden_dim, hidden_dim),
                                     nn.ReLU(),
                                     nn.Linear(hidden_dim, hidden_dim),
                                     nn.ReLU(),
                                     ])
        self.fc1 = nn.Linear(hidden_dim, out_dim)
        self.fc2 = nn.Linear(hidden_dim, out_dim)
        self.double()

    def forward(self, x):
        for layer in self.layers:
            x = layer(x)
        # return mean and logvariances
        return self.fc1(x), self.fc2(x)



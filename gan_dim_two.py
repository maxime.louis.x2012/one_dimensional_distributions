import torch
from torch import optim
import numpy as np
from networks import Generator, Critic
import matplotlib.pyplot as plt
import os
from utils import generate_normal_noise, generate_uniform_noise, compute_geodesic, get_embedding_space_positions, plot_latent_volume_form
from torch.utils.data import TensorDataset, DataLoader




figure_path = './two_dim'

data_dimension = 10
z_dimension = 2

batch_size = 64
nb_iterations = 100000
learning_rate = 5e-5

generate_noise = generate_uniform_noise

clamp_lower = -0.01
clamp_upper = 0.01


def sample_real_data(size=batch_size):
    noise_std = 0.01
    angles = np.random.uniform(0, 2 * np.pi, size=size)
    points = np.zeros((size, data_dimension))
    points[:, 0] = np.cos(angles)
    points[:, 1] = np.sin(angles)
    points = points + np.random.normal(0., noise_std, size=points.shape)

    return torch.from_numpy(points)
    # return next(iter(points_dataloader))[0]

points = sample_real_data(300).detach().numpy()
plt.scatter(points[:, 0], points[:, 1])
plt.savefig(os.path.join(figure_path, 'data.pdf'))

# Now that we have the data, we train a GAN to generate along this distribution
generator = Generator(in_dim=z_dimension, out_dim=data_dimension, hidden_dim=128)
critic = Critic(in_dim=data_dimension, hidden_dim=128)

optimizer_critic = optim.RMSprop(critic.parameters(), lr=learning_rate)
optimizer_generator = optim.RMSprop(generator.parameters(), lr=learning_rate)

torch.manual_seed(0)
np.random.seed(0)

for iteration in range(nb_iterations):

    if iteration == 0:
        n_critic = 500
    else:
        n_critic = 5

    for _ in range(n_critic):
        optimizer_critic.zero_grad()
        optimizer_generator.zero_grad()

        real_data = sample_real_data()

        # real_data = torch.from_numpy(np.random.normal(size=(batch_size, 1)))
        z = generate_noise(size=(len(real_data), z_dimension))
        fake_data = generator(z)
        critic_loss = torch.mean(critic(fake_data)) - torch.mean(critic(real_data))
        critic_loss.backward()

        optimizer_critic.step()

        # Clamping
        for p in critic.parameters():
            p.data.clamp_(clamp_lower, clamp_upper)

    optimizer_generator.zero_grad()
    optimizer_critic.zero_grad()

    z = generate_noise(size=(batch_size, z_dimension))
    G_sample = generator(z)
    D_fake = critic(G_sample)
    generator_loss = - torch.mean(D_fake)
    generator_loss.backward()

    optimizer_generator.step()

    if iteration % 50 == 0:
        print('Iteration {} Critic loss {}, generator loss {}'.format(iteration, critic_loss.detach().numpy(), generator_loss.detach().numpy()))

    if iteration % 100 == 0:
        z = generate_noise(size=(400, z_dimension))
        z_np = z.detach().numpy()
        fake_data = generator(z).detach().numpy()
        plt.clf()
        plt.scatter(fake_data[:, 0], fake_data[:, 1])
        n_points = 20
        for i in range(5):
            for j in range(5, 10):
                start = z[i].detach().numpy()
                end = z[j].detach().numpy()
                z_traj = [start + t/(n_points-1) * (end-start) for t in range(n_points)]
                z_traj = np.array(z_traj)
                z_traj_torch = torch.from_numpy(z_traj)
                data_traj = generator(z_traj_torch).detach().numpy()
                plt.plot(data_traj[:, 0], data_traj[:, 1])

        plt.savefig(os.path.join(figure_path, 'generated_data.pdf'))


    if iteration % 500 == 0:
        real_data = sample_real_data()
        embedding_positions = get_embedding_space_positions(generator, real_data, z_dimension)
        plt.clf()
        if z_dimension > 1:
            plt.scatter(embedding_positions[:, 0], embedding_positions[:, 1])
            # Now plotting some geodesics
            # for i in range(3):
            #     for j in range(4, 6):
            #         z_traj = compute_geodesic(embedding_positions[i, :], embedding_positions[j, :], generator)
            #         plt.plot(z_traj[:, 0], z_traj[:, 1])
            plt.savefig(os.path.join(figure_path, 'geodesics.pdf'))

            xmin, xmax = plt.xlim()
            ymin, ymax = plt.ylim()
            if z_dimension == 2:
                plot_latent_volume_form(xmin, xmax, ymin, ymax, generator) # only works in dim 2.
                plt.savefig(os.path.join(figure_path, 'gan_measure.pdf'))



# We get the volume form. We plot the pull-back volume form in the latent space it's
# the square determinant of the jacobian
# and the push-back volume form in the data space
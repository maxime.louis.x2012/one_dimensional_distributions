import torch
from torch import optim
import numpy as np
from networks import Generator, Encoder
import matplotlib.pyplot as plt
import os
from utils import generate_normal_noise, generate_uniform_noise, get_embedding_space_positions, compute_geodesic, plot_latent_volume_form
from torch.nn import functional as F

figure_path = './two_dim'

data_dimension = 10
z_dimension = 2

batch_size = 16
nb_iterations = 100000
learning_rate = 1e-5

generate_noise = generate_uniform_noise


def sample_real_data(size=batch_size):
    noise_std = 0.01
    angles = np.random.uniform(0, 2 * np.pi, size=size)
    points = np.zeros((size, data_dimension))
    points[:, 0] = np.cos(angles)
    points[:, 1] = np.sin(angles)
    points = points + np.random.normal(0., noise_std, size=points.shape)

    return torch.from_numpy(points)


def reparametrize(mean, logvariance):
    # return mean
    std = torch.exp(0.5 * logvariance)
    eps = torch.DoubleTensor(std.size()).normal_()
    return eps * std + mean

def vae_loss(reconstructed, data, mean, logvariance):
    # logvariance and mean are of shape (batch_size, z_dimension)
    BCE = F.mse_loss(reconstructed, data, size_average=False)
    KLD = -0.5 * torch.sum(1 + logvariance - mean**2 - torch.exp(logvariance))
    KLD /= batch_size * data_dimension
    return BCE + KLD


encoder = Encoder(in_dim=data_dimension, hidden_dim=128, out_dim=z_dimension)
decoder = Generator(in_dim=z_dimension, out_dim=data_dimension, hidden_dim=128)

optimizer = optim.Adam(list(encoder.parameters()) + list(decoder.parameters()), lr=learning_rate)

for iteration in range(nb_iterations):
    optimizer.zero_grad()

    data = sample_real_data(batch_size)
    # We encode the data
    mean, logvariance = encoder(data)
    z = reparametrize(mean, logvariance)
    reconstructed = decoder(z)

    loss = vae_loss(reconstructed, data, mean, logvariance)
    loss.backward()

    optimizer.step()

    if iteration % 50 == 0:
        print('Iteration {} Loss {}'.format(iteration, loss.detach().numpy()))
        # We also save some decoded data:
        decoded_data = reconstructed.detach().numpy()
        plt.clf()
        plt.scatter(decoded_data[:, 0], decoded_data[:, 1])
        # We now save some geodesics
        n_points = 20
        for i in range(5):
            for j in range(5, 10):
                start = z[i].detach().numpy()
                end = z[j].detach().numpy()
                z_traj = [start + t / (n_points - 1) * (end - start) for t in range(n_points)]
                z_traj = np.array(z_traj)
                z_traj_torch = torch.from_numpy(z_traj)
                data_traj = decoder(z_traj_torch).detach().numpy()
                plt.plot(data_traj[:, 0], data_traj[:, 1])
        plt.savefig(os.path.join(figure_path, 'vae_decoded.pdf'))

    if iteration % 1000 == 999:
        real_data = sample_real_data()
        embedding_positions = get_embedding_space_positions(decoder, real_data, z_dimension)
        plt.clf()
        if z_dimension > 1:
            plt.scatter(embedding_positions[:, 0], embedding_positions[:, 1])
            # Now plotting some geodesics
            # for i in range(3):
            #     for j in range(4, 6):
            #         z_traj = compute_geodesic(embedding_positions[i, :], embedding_positions[j, :], decoder)
            #         plt.plot(z_traj[:, 0], z_traj[:, 1])
            plt.savefig(os.path.join(figure_path, 'vae_latent.pdf'))

            xmin, xmax = plt.xlim()
            ymin, ymax = plt.ylim()
            if z_dimension == 2:
                plot_latent_volume_form(xmin, xmax, ymin, ymax, decoder)  # only works in dim 2.
                plt.savefig(os.path.join(figure_path, 'vae_measure.pdf'))





